const Koa = require('koa')
const Router = require('koa-router')
const fetch = require('node-fetch')

const app = new Koa()
const router = new Router()

router.get('/', async (ctx, next) => {
    const response = await fetch('https://splork.nerderium.com/message')
    const obj = await response.json()
	console.log(obj)
    ctx.body = {
        text: obj.message
    }
})

app.use(router.routes())
app.use(router.allowedMethods())

module.exports = app

if (!module.parent) app.listen(process.env.PORT || 2000)
